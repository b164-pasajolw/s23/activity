
// S23 ACTIVITY

// no.3
db.rooms.insertOne(
    {
	name: "single",
        accommodates: 2.0,
        price: 1000.0,
        description: "A simple room with all basic necessities.",
        roomsAvailable: 10.0,
        isAvailable: "false"
    } 
)

// no.4
db.rooms.insertMany([
    {
             name: "double",
                     accomodates: 3.0,
                     price: 2000.0,
                     description: "A room fit for a small family going on a vacation",
                     roomsAvailable: 5.0,
                     isAvailable: "false"        
            },
            {
             name: "family",
                     accomodates: 4.0,
                     price: 4000.0,
                     description: "A room with a queen sized bed perfect for a simple getaway",
                     roomsAvailable: 15.0,
                     isAvailable: "false"             
            }
    
 ])

// no.5
db.rooms.find({
    name: "double"
})

// no.6
db.rooms.updateOne({
    name: "family"},
    {
        $set: {
                     name: "queen",
                     accomodates: 4.0,
                     price: 4000.0,
                     description: "A room with a queen sized bed perfect for a simple getaway",
                     roomsAvailable: 0.0,
                     isAvailable: "false"
            }
});


// no.7
db.rooms.deleteMany({
    roomsAvailable: 0.0
});